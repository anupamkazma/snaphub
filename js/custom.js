$('#cat-1').owlCarousel({
    loop:true,
    autoplay:true,
    margin:20,
    dots:true,
    nav:false,
    items: 3,
    singleItem: true,
    responsive: {
        0:{
          items: 1
        },
        480:{
          items: 1
        },
        570:{
          items: 1
        },
        768:{
          items: 2
        },
        981:{
          items: 3
        }
    }
})
$('#cat-2').owlCarousel({
    loop:true,
    autoplay:true,
    margin:10,
    dots:false,
    nav:false,
    items: 1,
    singleItem: true
})




