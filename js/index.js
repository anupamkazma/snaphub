
function IsEmail(email) {
	var regex =
		/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (!regex.test(email)) {
		return false;
	} else {
		return true;
	}
}
function IsPhone(phone_number) {
	var regex = /^\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})$/;

	if (phone_number.length > 9 && regex.test(phone_number)) {
		return true;
	} else {
		return false;
	}
}
$(function () {
	$('#fname').blur(function () {
		$('#fname_error').hide();
	});
	$('#lname').blur(function () {
		$('#lname_error').hide();
	});
	$('#email').blur(function () {
		$('#email_error').hide();
		$('#invalid_email').hide();
	});
	$('#message').blur(function () {
		$('#message_error').hide();
	});
	$('#subject').blur(function () {
		$('#subject_error').hide();
	});
	
	$('#fname_error').hide();
	$('#lname_error').hide();
	$('#email_error').hide();
	$('#invalid_email').hide();
	$('#message_error').hide();
	
	$('#subject_error').hide();
	$('#submitbtn').click(function () {
		var error = false;
		var fname = $('#fname').val();
		var lname = $('#lname').val();
		var email = $('#email').val();
		var message = $('#message').val();
		var subject = $('#subject').val();

		if (fname == '') {
			$('#fname_error').show();
			error = true;
		}
		if (lname == '') {
			$('#lname_error').show();
			error = true;
		}
		if (email == '') {
			$('#email_error').show();
			error = true;
		} else if (email != '' && IsEmail(email) == false) {
			$('#email_error').hide();
			$('#invalid_email').show();
			error = true;
		} else {
			error = false;
		}

		if (message == '') {
			$('#message_error').show();
			error = true;
		}
		if (subject == '') {
			$('#subject_error').show();
			error = true;
		}
		if (error == true) {
			return false;
		}
		
		//ajax call php page
		$.post(
			'https://admin.snaphub.tech/pub/api/saveContact',
			$('#contactform').serialize(),
			function (response) {
				// $('#contactform').fadeOut('slow', function () {
					$('#txtMessage').html(response.message);
					$('#success').fadeIn('slow');
					$('#fname').val('');
					$('#lname').val('');
					 $('#email').val('');
					$('#message').val('');
					$('#subject').val('');
					setInterval(()=>{
						$('#success').fadeOut('slow', function () {
							$('#txtMessage').html('');
							$('#contactform').fadeIn('slow');
						});
					},2000)
				//});
			}
		);
		return false;
	});
	
});
$('#closeButton').click(function () {
	$('#success').fadeOut('slow', function () {
		$('#txtMessage').html('');
		$('#demoFrom').fadeIn('slow');
	});
});
$('#closeButtonContact').click(function () {
	$('#success').fadeOut('slow', function () {
		$('#txtMessage').html('');
		$('#contactform').fadeIn('slow');
	});
});



